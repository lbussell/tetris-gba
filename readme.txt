 _____    _        _     
|_   _|__| |_ _ __(_)___ 
  | |/ _ \ __| '__| / __|
  | |  __/ |_| |  | \__ \
  |_|\___|\__|_|  |_|___/
=========================

Fit the pieces together on the bottom of the stage (the "bucket").
If you fill all the blocks on a single row, that row will disappear!
You earn points for clearing lines.
The more lines you clear, the faster the pieces will fall.
Fast-falling your pieces also earns you more ponits!

          Controls
.===========================.
| Left   | Move piece left  |
| Right  | Move piece right |
| Down   | Fast-fall        |
| A/B    | Rotate piece     |
| Select | Restart          |
`===========================`

Tetrominoes
===========
"I"
 O   "O"    "T"    "J"   "L"    "S"     "Z"
 O                   O   O
 O   O O   O O O     O   O       O O   O O
 O   O O     O     O O   O O   O O       O O
